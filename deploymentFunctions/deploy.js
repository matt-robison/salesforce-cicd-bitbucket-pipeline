/***********************
Author: Matt Robison
Version: 1.1
Date: 12th Sep 2021
Description: Executes delta deployments to Salesforce Org from a Git Repository
***********************/

const utils = require('./utils.js');

//global configs
const KEY_FILE_LOCATION = 'server.key'; //location of certificate key file in repository
const DEFAULT_LOGIN_URL = 'https://test.salesforce.com'

//load all dependencies
utils.setup();

//get arguments from command line
const args = require('minimist')(process.argv.slice(2))

const USERNAME = args['username'];
const BRANCH = args['branch'];
const DEPLOY_BRANCH = args['deployBranch']; //optional parameter to mandate which branch a pipeline is run from
const CONSUMER_KEY = args['consumerKey'];
let LOGIN_URL = args['loginURL'];
const CHECK_DEPLOY = args['checkDeploy'];

//validate inputs
if(!USERNAME) {utils.errorMsgAndExit('Username not passed')}
if(!BRANCH) {utils.errorMsgAndExit('Branch not passed')}
if(!CONSUMER_KEY) {utils.errorMsgAndExit('Consumer key not passed')}
if(!LOGIN_URL) {LOGIN_URL = DEFAULT_LOGIN_URL};

//If DEPLOY_BRANCH passed, check to ensure deploying from the mandated branch
if(DEPLOY_BRANCH != undefined && DEPLOY_BRANCH !== BRANCH) {
    utils.errorMsgAndExit('Deploying from incorrect branch ' + BRANCH + '. Can only deploy from ' + DEPLOY_BRANCH);
}

utils.authenticateOrg(KEY_FILE_LOCATION, CONSUMER_KEY, USERNAME, LOGIN_URL);

//get current version of the org
let previousDeployedCommitVersion = utils.getCurrentVersionFromOrg(USERNAME);
let commitVersionToDeploy = utils.getMostRecentCommitFromRepo();

if(previousDeployedCommitVersion != '') {
    //get any files changed to be deployed
    let changedFiles = utils.getChangedFiles(previousDeployedCommitVersion, commitVersionToDeploy);
    
    if(changedFiles.length > 0) {
        //create deployment folder with changed files
        utils.createDeploymentFolder(changedFiles);
        //deploy the new folder to the org
        utils.deployFiles(USERNAME, CHECK_DEPLOY);
        //update commit version
        utils.updateCommitVersion(USERNAME, commitVersionToDeploy, BRANCH);
    } else {
        console.log('=== No files to deploy');
    }
} else {
    //no records in Release_Log__c to upgrade from, add current version as a base to start
    utils.updateCommitVersion(USERNAME, commitVersionToDeploy, BRANCH);
    utils.errorMsgAndExit(`Org has no records in Release_Log__c to upgrade from. Version ${commitVersionToDeploy} added to Release_Log__c as starting point. Commit metadata changes and re-run pipline to upgrade.`);
}

utils.successMsg('Deployment => ' + commitVersionToDeploy);