/********************
Author: Matt Robison
Version: 1.11
Date: 12th Sep 2021
Description: Helper functions to support deploying via Git/SFDX 
*********************/

//require libraries
const shell = require('shelljs');
const path = require('path');

//console colours
const CONSOLE_RED = '"\x1b[31m"';
const CONSOLE_GREEN = "\x1b[32m";

//list any cmd dependencies that need to be initially run
const DEPENDENCIES = ['npm i minimist'];

//temp folder to be used to hold the deployment files
const DEPLOY_FOLDER = 'deploy';

//initialise any prerequisite
module.exports.setup = function () { 
    DEPENDENCIES.forEach((dependency) => {
        this.executeCmd(dependency, true);
    })
}

//initialise any prerequisite
module.exports.authenticateOrg = function (keyFile, consumerKey, username, loginURL) { 
    let authenticate = `sfdx force:auth:jwt:grant -f ${keyFile} -i ${consumerKey} -u ${username} -d -s -r ${loginURL}`;

    this.executeCmd(authenticate, true);
}

//get the commit version the org is currently on
module.exports.getCurrentVersionFromOrg = function (username) { 
    var lastCommitVersion = '';

    //query the Release_Log__c for last deployed commit version
    let getReleaseLog = `sfdx force:data:soql:query -q "Select Commit__c from Release_Log__c Order By Deployment_Date__c DESC limit 1" --targetusername ${username} --json`;
    let outputReleaseLog = this.executeCmd(getReleaseLog, true);

    let objJSON = JSON.parse(outputReleaseLog);

    if(objJSON.status !== 0) {
        this.errorMsgAndExit('Release_Log__c SOQL failed - ' + objJSON.message)
    } else if(objJSON.result.records.length > 0) {
        //extract the commit version from the result
        lastCommitVersion = objJSON.result.records[0].Commit__c;
    }

    return lastCommitVersion
};

//workout latest commit version number from current branch
module.exports.getMostRecentCommitFromRepo = function () { 
    //get latest commit to deploy from current branch
    let getMostRecentCommit = `git rev-parse --short HEAD`;
    let outputMostRecentCommit = this.executeCmd(getMostRecentCommit, true);

    outputMostRecentCommit = outputMostRecentCommit.trim();

    return outputMostRecentCommit;
};

//get list of files to deploy between 2 commit versions
module.exports.getChangedFiles = function (versionFrom, versionTo) { 
    //get all files changed since a commit version
    let getFileChanges = `git diff --name-only ${versionFrom} ${versionTo}`;
    let outputChangedFiles = this.executeCmd(getFileChanges, true);

    //each file is on a new line, create array of the different files jjrtmr
    let changedFiles = outputChangedFiles.split("\n");

    //clean out any non deployable files
    var changedFilesCleaned = changedFiles.filter(function (filename) {
        return filename.includes('force-app/main/default');
    });

    return changedFilesCleaned;
};

//copy required files into a new deployment folder
module.exports.createDeploymentFolder = function (filesToDeploy) {
    console.log('=== Copying files to deploy');
    //loop through each file and copy to a new deployment folder
    filesToDeploy.forEach((file) => {
        let fileDeployLocation = DEPLOY_FOLDER + '/' + file;

        //create folder for file
        shell.mkdir('-p', path.dirname(fileDeployLocation));

        //copy file over
        console.log('copying --> ' + fileDeployLocation);
        shell.cp('-rn', file, fileDeployLocation);

        //if LWC or Aura, need whole folder
        if(file.includes('force-app/main/default/lwc/') || file.includes('force-app/main/default/aura/')) {
            //loop each file in the folder and copy it
            shell.ls(path.dirname(file + '/*')).forEach((copyFile) => {
                shell.cp('-rn', copyFile, DEPLOY_FOLDER + '/' + copyFile);
            });
        } else if (shell.test('-f', file + '-meta.xml')) { //check for associated -meta.xml file to add
            console.log('copying --> ' + fileDeployLocation + '-meta.xml');
            shell.cp('-rn', file + '-meta.xml', fileDeployLocation + '-meta.xml');
        }
    });
};

//deploy to Org
module.exports.deployFiles = function (username, checkDeploy) {
    let cmdCheckDeploy = '';

    //run a check only deploy, allowing a quick deploy
    if(checkDeploy === 'yes') {
        cmdCheckDeploy = '--checkonly';
    }

    let deployFiles = `sfdx force:source:deploy --sourcepath ${DEPLOY_FOLDER} ${cmdCheckDeploy} --targetusername ${username}`;
    console.log('=== Deploying');

    this.executeCmd(deployFiles, false);
};

//update commit version to Org
module.exports.updateCommitVersion = function (username, commitVersion, branch) {
    let deploymentDate = new Date().toISOString();
    let updateCommitVersion = `sfdx force:data:record:create -s Release_Log__c -v "Commit__c='${commitVersion}' Branch__c='${branch}' Deployment_Date__c='${deploymentDate}'" --targetusername ${username}`;

    this.executeCmd(updateCommitVersion, true);
};

//execute a CLI command
module.exports.executeCmd = function (cmd, silent) {
    console.log(cmd);

    var params = {};
    if(silent) { params.silent = true; }

    const { stdout, stderr, code } = shell.exec(cmd, params);

    if(code === 0) {
        return stdout;
    } else {
        this.errorMsgAndExit(stderr);
    }
};

//update commit version to Org
module.exports.errorMsgAndExit = function (message) {
    console.log(CONSOLE_RED, '=== ERROR: ' + message);
    process.exit(1);
};

//update commit version to Org
module.exports.successMsg = function (message) {
    console.log(CONSOLE_GREEN, '=== SUCCESS: ' + message);
};